data "aws_region" "current" {}

# Create VPC
resource "aws_vpc" "efira-eba" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "efira-eba-vpc"
  }
}

# Create subnets
resource "aws_subnet" "subnet1" {
  vpc_id            = aws_vpc.efira-eba.id
  cidr_block        = "10.0.0.0/24"
  availability_zone = "${data.aws_region.current.name}a"

  tags = {
    Name = "efira-eba-subnet1"
  }
}

resource "aws_subnet" "subnet2" {
  vpc_id            = aws_vpc.efira-eba.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "${data.aws_region.current.name}b"

  tags = {
    Name = "efira-eba-subnet2"
  }
}

resource "aws_db_subnet_group" "efira-eba_db_subnet_group" {
  name       = "efira-eba"
  subnet_ids = [aws_subnet.subnet1.id, aws_subnet.subnet2.id]
  tags = {
    Name = "efira-eba"
  }
}

# Create security group for database instances
resource "aws_security_group" "efira-eba_db_sg" {
  name        = "efira-eba-db-security-group"
  description = "Efira-eba DB security group"
  vpc_id      = aws_vpc.efira-eba.id

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Adjust for secure access
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create Microsoft SQL Server instance
resource "aws_db_instance" "efira-eba_sqlserver" {
  identifier                = "efira-eba-sqlserver-instance"
  engine                    = "sqlserver-se"
  engine_version            = "14.00.3411.3.v1"
  instance_class            = "db.t3.medium"
  publicly_accessible       = false
  storage_type              = "gp2"
  allocated_storage         = 100
  max_allocated_storage     = 1000
  backup_retention_period   = 7
  maintenance_window        = "Mon:00:00-Mon:03:00"
  #preferred_backup_window   = "01:00-02:00"
  skip_final_snapshot       = true
  apply_immediately         = true
  vpc_security_group_ids    = [aws_security_group.efira-eba_db_sg.id]
  db_subnet_group_name      = aws_db_subnet_group.efira-eba_db_subnet_group.name
  multi_az                  = false
  storage_encrypted         = false

  tags = {
    Name = "efira-eba-sqlserver"
  }
}

# Create DynamoDB table
resource "aws_dynamodb_table" "efira-eba_table" {
  name           = "efira-eba-dynamodb-table"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "id"
  read_capacity  = 5
  write_capacity = 5

  attribute {
    name = "id"
    type = "N"
  }

  attribute {
    name = "name"
    type = "S"
  }

  tags = {
    Name = "efira-eba-dynamodb-table"
  }
}

# Create S3 bucket
resource "aws_s3_bucket" "efira-eba_bucket" {
  bucket = "efira-eba-bucket"
  acl    = "private"

  tags = {
    Name = "efira-eba-s3-bucket"
  }
}

# Create VPC endpoints
resource "aws_vpc_endpoint" "sqlserver" {
  vpc_id              = aws_vpc.efira-eba.id
  service_name        = "com.amazonaws.${data.aws_region.current.name}.rds-db"
  vpc_endpoint_type   = "Interface"
  security_group_ids  = [aws_security_group.efira-eba_db_sg.id]
  subnet_ids          = [aws_subnet.subnet1.id, aws_subnet.subnet2.id]
}

resource "aws_vpc_endpoint" "dynamodb" {
  vpc_id              = aws_vpc.efira-eba.id
  service_name        = "com.amazonaws.${data.aws_region.current.name}.dynamodb"
  vpc_endpoint_type   = "Interface"
  security_group_ids  = [aws_security_group.efira-eba_db_sg.id]
  subnet_ids          = [aws_subnet.subnet1.id, aws_subnet.subnet2.id]
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id              = aws_vpc.efira-eba.id
  service_name        = "com.amazonaws.${data.aws_region.current.name}.s3"
  vpc_endpoint_type   = "Gateway"
}

resource "aws_dms_replication_subnet_group" "efira_eba_dms_subnet_group" {
  replication_subnet_group_id = "efira-eba-dms-subnet-group"
  replication_subnet_group_description = "EFIRA EBA DMS Subnet Group"
  subnet_ids = [aws_subnet.subnet1.id, aws_subnet.subnet2.id]
  
  tags = {
    Name = "efira-eba-dms-subnet-group"
  }
}



# Create the DMS replication task
resource "aws_dms_replication_task" "efira-eba_dms_task" {
  replication_task_id             = "efira-eba-dms-task"
  migration_type                          = "full-load"
  source_endpoint_arn                     = aws_dms_endpoint.efira-eba_source.arn
  target_endpoint_arn                     = aws_dms_endpoint.efira-eba_target.arn
  table_mappings                          = file("table-mappings.json") # Update with your table mappings file path
  replication_instance_arn                = aws_dms_replication_instance.efira-eba_dms_instance.arn
  replication_subnet_group_id             = aws_dms_replication_subnet_group.efira-eba_dms_subnet_group.id
  migration_settings                      = file("migration-settings.json") # Update with your migration settings file path

  tags = {
    Name = "efira-eba-dms-task"
  }
}
# Create DMS source endpoint
resource "aws_dms_endpoint" "efira-eba_source" {
  endpoint_id = "efira-eba-source-endpoint"
  endpoint_type       = "source"
  engine_name         = "sqlserver"
  username            = var.source_db_username
  password            = var.source_db_password
  server_name         = aws_db_instance.efira-eba_sqlserver.address
  database_name       = var.source_db_name
}

# Create DMS target endpoint
resource "aws_dms_endpoint" "efira-eba_target" {
  endpoint_id = "efira-eba-target-endpoint"
  endpoint_type       = "target"
  engine_name         = "dynamodb"
  service_access_role = aws_iam_role.efira-eba_dms_role.arn
  extra_connection_attributes = "dynamoDbSettings={{ServiceAccessRoleArn=${aws_iam_role.efira-eba_dms_role.arn}}}"
}

# IAM role for DMS service access
resource "aws_iam_role" "efira-eba_dms_role" {
  name = "efira-eba-dms-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "dms.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

# Attach policy to IAM role
resource "aws_iam_role_policy_attachment" "efira-eba_dms_role_policy" {
  role       = aws_iam_role.efira-eba_dms_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonDMSRedshiftS3Role"
}
