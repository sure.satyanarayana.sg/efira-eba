variable "source_db_password" {
  description = "Password for the source database"
  type        = string
  default = "usewithcaution"
}

variable "source_db_username" {
  description = "Name of source database"
  type        = string
  default = "efira-stp"
}

variable "source_db_name" {
  description = "Name of source database"
  type        = string
  default = "efira-eba"
}